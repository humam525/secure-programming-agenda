<?php
session_start();

// Mengakhiri semua sesi
session_unset();
session_destroy();

// Mengarahkan kembali ke halaman login
header("Location: login noCaptcha.php");
exit();
?>
