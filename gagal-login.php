<?php
session_start();

// Set session waktu mulai tunggu
if (!isset($_SESSION['wait_start'])) {
    $_SESSION['wait_start'] = time();
}

// Cek waktu tunggu
$current_time = time();
$wait_time = 180; // 3 menit
$elapsed_time = $current_time - $_SESSION['wait_start'];

// Cek apakah waktu tunggu sudah berakhir
if ($elapsed_time >= $wait_time) {
    // Reset session dan arahkan ke halaman login
    unset($_SESSION['wait_start']);
    header("Location: login.php");
    exit();
}

// Hitung sisa waktu tunggu
$remaining_time = $wait_time - $elapsed_time;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Waiting...</title>
</head>
<body>
    <div style="text-align:center; margin-top: 50px;">
        <h2>Anda telah mencoba login lebih dari 3 kali.</h2>
        <p>Silakan tunggu <?php echo $remaining_time; ?> detik sebelum mencoba login kembali.</p>
    </div>
</body>
</html>