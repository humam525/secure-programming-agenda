<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
        <label for="username">Username</label>
        <input type="text" name="username" id=""><br>
        <label for="password">Password</label>
        <input type="password" name="password" id="">
        <input type="submit" value="kirim"><br>
    </form>
</body>
</html>

<?php
session_start(); // Mulai session

require_once ('connection/db-conect.php');

// Inisialisasi jumlah percobaan login
$login_attempts = 0;

// Cek apakah variabel session untuk percobaan login sudah di-set
if (isset($_SESSION['login_attempts'])) {
    $login_attempts = $_SESSION['login_attempts'];
}

// Langkah 1: Validasi request method
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Langkah 2: Ambil variabel username dan password
    $username = htmlspecialchars($_POST["username"]);
    $password = htmlspecialchars($_POST["password"]);

    // Langkah 3: Pastikan kedua variabel memiliki nilai
    if (!empty($username) && !empty($password)) {
        // Langkah 4: Buat koneksi ke MySQL
        $conn = connect_db();
        $conn = new mysqli($servername, $db_username, $db_password, $dbname);
        // Menambahkan delay 5 detik
        sleep(3);
        
        // Check koneksi
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Langkah 5: Buat query untuk mengambil row dari database berdasarkan username dan password

        // mendefinisikan prepared statement dengan placeholder ? untuk parameter.
        $stmt = $conn->prepare("SELECT * FROM pelanggan WHERE nama = ? AND Password = ?");
        
        // "ss" berarti kedua parameter adalah string
        $stmt->bind_param("ss", $username, $password);
        // Eksekusi prepared statement
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            // Langkah 6: Jika ditemukan, redirect ke homepage
            header("Location: agenda.php");
            exit();
        } else {
            // Jika tidak ditemukan, tambahkan jumlah percobaan login
            $login_attempts++;
            $_SESSION['login_attempts'] = $login_attempts;

            if ($login_attempts >= 3) {
                // Jika percobaan login lebih dari atau sama dengan 3, redirect ke halaman lain dan hentikan session
                header("Location: gagal-login.php");
                session_destroy(); // Hentikan session
                exit();
            } else {
                // Jika percobaan login kurang dari 3, tampilkan pesan kesalahan
                echo "Username atau password salah. Silakan coba lagi.";
            }
        }
        // Tutup prepared statement
        $stmt->close();

        // Tutup koneksi
        $conn->close();
    } else {
        // Jika salah satu variabel tidak memiliki nilai, tampilkan pesan kesalahan
        echo "Username dan password harus diisi.";
    }
}
?>