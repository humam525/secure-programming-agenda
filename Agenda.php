<?php
session_start();

// Fungsi untuk membersihkan output untuk menghindari XSS
function clean_output($data) {
    return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
}

// Check if the user is logged in
if (!isset($_SESSION['loggedin'])) {
    header('Location: login noCaptcha.php');
    exit;
}

require_once('connection/db-conect.php');

if ($_SERVER['REQUEST_METHOD'] === "POST" && isset($_POST['delete_agenda'])) {
    if (!empty($_POST['agenda_id'])) {
        $agenda_id = intval($_POST['agenda_id']); // memastikan ID adalah integer
        $con = connect_db();

        // Kueri SQL untuk menghapus agenda berdasarkan ID
        $query_delete = "DELETE FROM Agenda WHERE agenda_id = ?";
        $stmt_delete = mysqli_prepare($con, $query_delete);
        mysqli_stmt_bind_param($stmt_delete, "i", $agenda_id);

        // Jalankan kueri penghapusan
        if (mysqli_stmt_execute($stmt_delete)) {
            // Jika penghapusan berhasil, refresh halaman
            echo "<meta http-equiv='refresh' content='0'>";
            exit;
        } else {
            // Jika penghapusan gagal
            echo "<p>Gagal menghapus agenda</p>";
        }
        // Tutup pernyataan dan koneksi
        mysqli_stmt_close($stmt_delete);
        mysqli_close($con);
    } else {
        // Jika ID agenda tidak valid
        echo "<p>ID agenda tidak valid</p>";
    }
}

// Tampilkan daftar agenda
if ($_SERVER['REQUEST_METHOD'] !== "GET") {
    http_response_code(405);
    echo '<p>Invalid request method</p>';
    exit;
}

$selected_month = '';
$con = connect_db();
if (isset($_GET['bulan'])) {
    $selected_month = $_GET['bulan'];
}

// SQL query
$query = "SELECT * FROM Agenda WHERE MONTH(WaktuAgenda) = ?;";
$stmt = mysqli_prepare($con, $query);
mysqli_stmt_bind_param($stmt, "s", $selected_month);
mysqli_stmt_execute($stmt);
$result = mysqli_stmt_get_result($stmt);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agenda</title>
</head>
<body>
    <h1>Nama orang</h1>
    <form id="logoutForm" action="logout.php" method="post" style="display: none;">
        <input type="hidden" name="logout" value="1">
    </form>
    <button onclick="document.getElementById('logoutForm').submit();">Logout</button>

    <h3>Bulan</h3>
    <form action="<?php echo clean_output($_SERVER['PHP_SELF']); ?>" method="get">
        <select name="bulan" id="bulan">
            <option value="1">Januari</option>
            <option value="2">Februari</option>
            <option value="3">Maret</option>
            <option value="4">April</option>
            <option value="5">Mei</option>
            <option value="6">Juni</option>
            <option value="7">Juli</option>
            <option value="8">Agustus</option>
            <option value="9">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
        </select>
        <input type="submit" value="Cari Agenda" name="from">
    </form>

    <button onclick="showAddAgenda()">Tambah Agenda</button>

    <h2>Agenda Harian</h2>
    <table style="border: 1px solid black; border-collapse: collapse;">
        <thead>
            <tr>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Nama Agenda</th>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Tanggal</th>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Jam</th>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!$result) {
                echo "<p>Tidak ada hasil</p>";
            } else {
                while ($row = mysqli_fetch_assoc($result)) {
                    $nama_agenda = clean_output($row['nama_agenda']);
                    $tanggal_agenda = date('Y-m-d', strtotime($row['WaktuAgenda']));
                    $jam_agenda = date('H:i', strtotime($row['WaktuAgenda']));
                    $agenda_id = intval($row['agenda_id']); // memastikan ID adalah integer

                    // Handle invalid dates
                    if ($tanggal_agenda == "1970-01-01") {
                        $tanggal_agenda = "-";
                        $jam_agenda = "-";
                    }

                    echo "<tr>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">' . $nama_agenda . "</td>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">' . $tanggal_agenda . "</td>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">' . $jam_agenda . "</td>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">';
                    echo '<form action="' . clean_output($_SERVER['PHP_SELF']) . '" method="post" style="display:inline-block; margin-right: 5px;">';
                    echo '<input type="hidden" name="agenda_id" value="' . $agenda_id . '">';
                    echo '<button type="submit" name="delete_agenda">Delete</button>';
                    echo '</form>';
                    echo '<form action="edit-agenda.php" method="get" style="display:inline-block;">';
                    echo '<input type="hidden" name="agenda_id" value="' . $agenda_id . '">';
                    echo '<button type="submit">Edit</button>';
                    echo '</form>';
                    echo "</td>";
                    echo "</tr>";
                }
            }
            mysqli_free_result($result);
            mysqli_stmt_close($stmt);
            mysqli_close($con);
            ?>
        </tbody>
    </table>

    <script>
        function showAddAgenda() {
            // Dynamically create a form to redirect to "Tambah-agenda.php"
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "Tambah-agenda.php");
            document.body.appendChild(form);
            form.submit();
        }
    </script>
</body>
</html>
