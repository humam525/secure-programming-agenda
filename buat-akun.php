<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Create an Account</h1>

    <?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once('connection/db-conect.php');

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = connect_db();

        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $nama = mysqli_real_escape_string($conn, $_POST['nama']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $confirm_password = mysqli_real_escape_string($conn, $_POST['confirm_password']);

        // Validasi data formulir
        if (empty($nama) || empty($password) || empty($confirm_password)) {
            echo "<div id='message'>All fields are required.</div>";
        } elseif ($password !== $confirm_password) {
            echo "<div id='message'>Passwords do not match.</div>";
        } else {
            // Hash the password
            $hashed_password = password_hash($password, PASSWORD_DEFAULT);

            // Insert into database
            $sql = "INSERT INTO pelanggan (nama, Password) VALUES (?, ?)";
            $stmt = mysqli_prepare($conn, $sql);

            if ($stmt) {
                mysqli_stmt_bind_param($stmt, "ss", $nama, $hashed_password);

                if (mysqli_stmt_execute($stmt)) {
                    echo "<div id='message'>Account created successfully!</div>";
                } else {
                    echo "<div id='message'>Error: " . mysqli_stmt_error($stmt) . "</div>";
                }

                mysqli_stmt_close($stmt);
            } else {
                echo "<div id='message'>Error preparing statement: " . mysqli_error($conn) . "</div>";
            }

            mysqli_close($conn);
        }
    }
    ?>

    <form method="post" action="">
        <div>
            <label for="nama">Username</label>
            <input type="text" name="nama" id="nama" required><br>
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" required><br>
        </div>
        <div>
            <label for="confirm_password">Confirm Password</label>
            <input type="password" name="confirm_password" id="confirm_password" required><br>
        </div>
        <button type="submit">Register</button>
    </form>
</body>
</html>
