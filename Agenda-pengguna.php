<?php
session_start();

// Check jika penmgguan sudah login jika tidak akan ke halaman login kembali
if (!isset($_SESSION['loggedin'])) {
    header('Location: login noCaptcha.php');
    exit;
}

require_once('connection/db-conect.php');

// Tampilkan daftar agenda
if ($_SERVER['REQUEST_METHOD'] !== "GET") {
    http_response_code(405);
    $message = 'Invalid request method';
    exit;
}

$selected_month = '';
$con = connect_db();
if (isset($_GET['bulan'])) {
    $selected_month = intval($_GET['bulan']); // Ensure the month is an integer to prevent SQL injection
}

// SQL query
$query = "SELECT * FROM Agenda WHERE MONTH(WaktuAgenda) = ?;";
$stmt = mysqli_prepare($con, $query);
$stmt->bind_param("i", $selected_month); // gunakan 'i' untuk integer binding sql injaction
$stmt->execute();
$result = $stmt->get_result();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agenda</title>
</head>
<body>
    <h1>Nama orang</h1>
    <form id="logoutForm" action="logout.php" method="post" style="display: none;">
        <input type="hidden" name="logout" value="1">
    </form>
    <button type="button" onclick="document.getElementById('logoutForm').submit();">Logout</button>

    <h3>Bulan</h3>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="get">
        <select name="bulan" id="bulan">
            <option value="1">Januari</option>
            <option value="2">Februari</option>
            <option value="3">Maret</option>
            <option value="4">April</option>
            <option value="5">Mei</option>
            <option value="6">Juni</option>
            <option value="7">Juli</option>
            <option value="8">Agustus</option>
            <option value="9">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
        </select>
        <input type="submit" value="Cari Agenda" name="from">
    </form>

    <h2>Agenda Harian</h2>
    <table style="border: 1px solid black; border-collapse: collapse;">
        <thead>
            <tr>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Nama Agenda</th>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Tanggal</th>
                <th style="border: 1px solid black; padding: 8px; text-align: left;">Jam</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!$result) {
                echo "<p>Tidak ada hasil</p>";
            } else {
                while ($row = $result->fetch_assoc()) {
                    $nama_agenda = htmlspecialchars($row['nama_agenda'], ENT_QUOTES, 'UTF-8');
                    $tanggal_agenda = htmlspecialchars(date('Y-m-d', strtotime($row['WaktuAgenda'])), ENT_QUOTES, 'UTF-8');
                    $jam_agenda = htmlspecialchars(date('H:i', strtotime($row['WaktuAgenda'])), ENT_QUOTES, 'UTF-8');
                    $agenda_id = intval($row['agenda_id']); //Pastikan ID adalah bilangan bulat

                    echo "<tr>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">' . $nama_agenda . "</td>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">' . $tanggal_agenda . "</td>";
                    echo '<td style="border: 1px solid black; padding: 8px; text-align: left;">' . $jam_agenda . "</td>";
                    echo "</tr>";
                }
            }
            $result->free_result();
            $stmt->close();
            $con->close();
            ?>
        </tbody>
    </table>
</body>
</html>
