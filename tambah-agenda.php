<?php
session_start();

// Fungsi untuk membersihkan output untuk menghindari XSS
function clean_output($data) {
    return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
}

// Check if the user is logged in
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('Location: login.php');
    exit;
}

// Menangani pengiriman formulir
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('connection/db-conect.php');

    if (isset($_POST['nama_agenda']) && isset($_POST['tanggal']) && isset($_POST['jam'])) {
        $conn = connect_db();
        $nama_agenda = mysqli_real_escape_string($conn, $_POST['nama_agenda']);
        $tanggal = mysqli_real_escape_string($conn, $_POST['tanggal']);
        $jam = mysqli_real_escape_string($conn, $_POST['jam']);

        $WaktuAgenda = "$tanggal $jam";

        $sql = "INSERT INTO Agenda (nama_agenda, WaktuAgenda) VALUES (?, ?)";
        $stmt = mysqli_prepare($conn, $sql);

        mysqli_stmt_bind_param($stmt, "ss", $nama_agenda, $WaktuAgenda);

        if (mysqli_stmt_execute($stmt)) {
            $_SESSION['message'] = "Agenda berhasil ditambahkan!";
        } else {
            $_SESSION['message'] = "Error: " . mysqli_stmt_error($stmt);
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);

        // Redirect untuk menghindari pengiriman ulang formulir
        header('Location: tambah-agenda.php');
        exit();
    } else {
        $_SESSION['message'] = 'Data tidak lengkap.';
        header('Location: tambah-agenda.php');
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Agenda</title>
</head>
<body>
    <h1>Tambah Agenda</h1>
    <?php
    if (isset($_SESSION['message'])) {
        echo "<div id='message'>" . clean_output($_SESSION['message']) . "</div>";
        unset($_SESSION['message']);
    }
    ?>
    <form method="post" action="">
        <div>
            <label for="nama-agenda">Nama Agenda</label>
            <input type="text" name="nama_agenda" id="nama-agenda" required><br>
        </div>

        <div>
            <label for="tanggal">Pilih Tanggal:</label>
            <input type="date" id="tanggal" name="tanggal" required>
        </div>

        <div>
            <label for="jam">Pilih Jam:</label>
            <input type="time" id="jam" name="jam" required>
        </div>

        <button type="submit">Tambah Agenda</button>
    </form>
</body>
</html>
