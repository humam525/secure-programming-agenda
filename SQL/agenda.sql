CREATE TABLE Agenda(
    agenda_id INT NOT NULL AUTO_INCREMENT,
    nama_agenda VARCHAR(100),
    WaktuAgenda DATETIME,
    userID INT,
    PRIMARY KEY (agenda_id),
    Foreign Key (userID) REFERENCES pelanggan(userID) ON UPDATE CASCADE ON DELETE CASCADE
);

