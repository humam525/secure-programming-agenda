<?php 

$servername = "localhost";
$db_username = "root";
 $db_password = "";
$dbname = "agenda"; //tolong diubah sesuai nama yang digunakan

function connect_db(){
    global $servername, $db_username, $db_password, $dbname;
    $conn = new mysqli($servername, $db_username, $db_password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed". $conn->connect_error);
    }   

    return $conn;

}