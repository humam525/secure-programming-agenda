<?php
require_once('connection/db-conect.php');

if ($_SERVER['REQUEST_METHOD'] === "GET" && isset($_GET['agenda_id'])) {
    $agenda_id = $_GET['agenda_id'];
    $con = connect_db();

    // Ambil data agenda berdasarkan ID
    $query = "SELECT * FROM Agenda WHERE agenda_id = ?";
    $stmt = mysqli_prepare($con, $query);
    mysqli_stmt_bind_param($stmt, "i", $agenda_id);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $nama_agenda = $row['nama_agenda'];
        $tanggal_agenda = date('Y-m-d', strtotime($row['WaktuAgenda']));
        $jam_agenda = date('H:i', strtotime($row['WaktuAgenda']));
    } else {
        echo "<p>Agenda tidak ditemukan</p>";
        exit;
    }

    mysqli_stmt_close($stmt);
    mysqli_close($con);
} elseif ($_SERVER['REQUEST_METHOD'] === "POST" && isset($_POST['update_agenda'])) {
    if (!empty($_POST['agenda_id']) && !empty($_POST['nama_agenda']) && !empty($_POST['tanggal_agenda']) && !empty($_POST['jam_agenda'])) {
        $agenda_id = $_POST['agenda_id'];
        $nama_agenda = $_POST['nama_agenda'];
        $tanggal_agenda = $_POST['tanggal_agenda'];
        $jam_agenda = $_POST['jam_agenda'];
        $waktu_agenda = $tanggal_agenda . ' ' . $jam_agenda;
        $con = connect_db();

        // Kueri SQL untuk mengedit agenda berdasarkan ID
        $query_update = "UPDATE Agenda SET nama_agenda = ?, WaktuAgenda = ? WHERE agenda_id = ?";
        $stmt_update = mysqli_prepare($con, $query_update);
        mysqli_stmt_bind_param($stmt_update, "ssi", $nama_agenda, $waktu_agenda, $agenda_id);

        // Jalankan kueri edit
        if (mysqli_stmt_execute($stmt_update)) {
            // Jika edit berhasil, redirect ke halaman utama
            header("Location: Agenda.php");
            exit;
        } else {
            echo "<p>Gagal mengedit agenda</p>";
        }

        mysqli_stmt_close($stmt_update);
        mysqli_close($con);
    } else {
        echo "<p>Data agenda tidak valid</p>";
    }
} else {
    echo "<p>ID agenda tidak valid</p>";
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Agenda</title>
</head>

<body>
    <h1>Edit Agenda</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <input type="hidden" name="agenda_id" value="<?php echo $agenda_id; ?>">
        <label for="nama_agenda">Nama Agenda:</label><br>
        <input type="text" id="nama_agenda" name="nama_agenda" value="<?php echo $nama_agenda; ?>"><br>
        <label for="tanggal_agenda">Tanggal:</label><br>
        <input type="date" id="tanggal_agenda" name="tanggal_agenda" value="<?php echo $tanggal_agenda; ?>"><br>
        <label for="jam_agenda">Jam:</label><br>
        <input type="time" id="jam_agenda" name="jam_agenda" value="<?php echo $jam_agenda; ?>"><br><br>
        <input type="submit" value="Simpan Perubahan" name="update_agenda">
    </form>
</body>

</html>
