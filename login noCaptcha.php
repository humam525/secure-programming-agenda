<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <script src="https://hcaptcha.com/1/api.js" async defer></script>
</head>
<body>
    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        <label for="username">Username</label>
        <input type="text" name="username" id="username" required><br>
        <label for="password">Password</label>
        <input type="password" name="password" id="password" required><br>
        <div class="h-captcha" data-sitekey="ac26bb58-26b8-466d-a5d9-ba1480808c42"></div> 
        <input type="submit" value="Kirim"><br>
    </form>
</body>
</html>

<?php
session_start(); // Mulai session

require_once('connection/db-conect.php');

// Fungsi untuk membersihkan input untuk menghindari XSS
function clean_input($data) {
    return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
}

// Inisialisasi jumlah percobaan login
$login_attempts = 0;

// Cek apakah variabel session untuk percobaan login sudah di-set
if (isset($_SESSION['login_attempts'])) {
    $login_attempts = $_SESSION['login_attempts'];
}

// Langkah 1: Validasi request method
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Validasi hCaptcha
    if (!isset($_POST['h-captcha-response'])) {
        echo "Harap selesaikan CAPTCHA.";
        exit;
    }

    $captcha = $_POST['h-captcha-response'];
    $secretKey = "ES_5ac847832f4042dc8051e6eb8d651215"; // Ganti dengan secret key 
    $response = file_get_contents("https://hcaptcha.com/siteverify?secret=$secretKey&response=$captcha");
    $responseKeys = json_decode($response, true);

    if (intval($responseKeys["success"]) !== 1) {
        echo "Harap selesaikan CAPTCHA.";
        exit;
    } 

    // Langkah 2: Ambil variabel username dan password
    $username = clean_input($_POST["username"]);
    $password = clean_input($_POST["password"]);

    // Langkah 3: Pastikan kedua variabel memiliki nilai
    if (!empty($username) && !empty($password)) {
        // Langkah 4: Buat koneksi ke MySQL
        $conn = connect_db();

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Menambahkan delay 2 detik
        sleep(2);

        // Langkah 5: Buat query untuk mengambil row dari database berdasarkan username dan password
        $stmt = $conn->prepare("SELECT * FROM pelanggan WHERE nama = ? AND Password = ?");
        $stmt->bind_param("ss", $username, $password);

        // Eksekusi prepared statement
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            // Langkah 6: Jika ditemukan, cek peran (role) dari user
            $row = $result->fetch_assoc();
            $_SESSION['loggedin'] = true;

            if (isset($row['Role'])) {
                if ($row['Role'] === 'Admin') {
                    // Redirect ke agenda.php jika role adalah Admin
                    header("Location: Agenda.php");
                } else if ($row['Role'] === 'Pengguna') {
                    // Redirect ke agenda-user.php jika role adalah Pengguna
                    header("Location: Agenda-pengguna.php");
                }
            } else {
                // Jika role tidak ditemukan, tampilkan pesan kesalahan
                echo "Peran (Role) pengguna tidak ditemukan.";
            }
            exit();
        } else {
            // Jika tidak ditemukan, tambahkan jumlah percobaan login
            $login_attempts++;
            $_SESSION['login_attempts'] = $login_attempts;

            if ($login_attempts >= 3) {
                // Jika percobaan login lebih dari atau sama dengan 3, redirect ke halaman lain dan hentikan session
                header("Location: gagal-login.php");
                session_destroy(); // Hentikan session
                exit();
            } else {
                echo "Username atau password salah. Silakan coba lagi.";
            }
        }

        // Tutup prepared statement
        $stmt->close();

        // Tutup koneksi
        $conn->close();
    } else {
        // Jika salah satu variabel tidak memiliki nilai, tampilkan pesan kesalahan
        echo "Username dan password harus diisi.";
    }
}
?>